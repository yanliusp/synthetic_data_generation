# Synthetic data generation for MDIOF development

## What are inside
The `Make_templates.py` is generated automatically from the `Make_Templates_and_Pulses.ipynb` 

The `Make_Templates_and_Pulses.ipynb` was used to:
1. generate a template (based on a function)
2. write the template to a ROOT file (that can be read by CDMSBats)
3. generate pulses with a fixed pulse shape (based on a function)
4. generate noise based on different noise models (white, 1/f, realistic, etc.)
5. combine pulse and noise to make synthetic waveforms, and write events to midas files.
	- this is done in conjuction with [SuperSalt](https://gitlab.com/supercdms/DataHandling/SuperSalt) repository (currently, the feature/writeForRuchi branch).

The `generate_synthetic_data.sh` bash script is used to generate **non-hybrid** dataset.

The `generate_synthetic_hybrid_data.sh` bash script is used to generate **hybrid** dataset.


## Prerequisites
1. Singularity image of the SCDMS offline release: [CDMS-V04-07](https://gitlab.com/supercdms/CompInfrastructure/ReleaseBuilder/container_registry).
	- You need to be inside the singularity to do most of the stuff here.
	- more documentation can be found on [confluence](https://confluence.slac.stanford.edu/display/CDMS/Using+CDMS+Offline+Software+Releases#UsingCDMSOfflineSoftwareReleases-Singularityimage)
2. [SuperSalt](https://gitlab.com/supercdms/DataHandling/SuperSalt):
	- SuperSalt is not yet a package in SCDMS offline release, so we need to git clone and install it manually. Please note that it is under intensive development, and you most likely need to checkout a feature branch.

## Dataset

A synthetic dataset is composed of both BORRs and physics triggers. We need the BORRs for noise PSD calculation, and physics triggers for OF fitting evaluation. Typically we want to generate at least 500 BORRs to make sure that we have enough statistics to minimize the noise variation (either intentional or statistical), and it is not uncommon to generate more.

In addition to the waveforms we also need to be careful with the ODB dumps. These can be understood as header files that define the structure of the event as well as other information of the event (such as trigger primitives, pulse settings, etc.). the easiest way is to copy the ODB dump from existing midas files (but you need to carefully pick them so that they have the settings you want), but sometimes they don't exist - which means that we will have to generate them (using [pyRawIO](https://gitlab.com/supercdms/DataHandling/pyRawIO)).

### Non-hybrid dataset

### Hybrid dataset
