#!/bin/bash

# make sure you are inside singularity CDMS-V04-12

# using SuperSalt-???

# stop when error
set -e

#RAW_DATA_PATH=/sdf/home/y/yanliu/data/HVDMC/51230510_000075
cd /sdf/home/y/yanliu/software/SuperSalt
make clean
make

# For noise triggers
for run in {1..25}
do
  echo "Generating dump $((0 + run))"
  python3 /sdf/home/y/yanliu/notebooks/synthetic/ruchi/Make_templates.py
  pwd
  ./AddSalt.exe /sdf/home/y/yanliu/data/raw/21210907_152363_F0001.mid.gz /sdf/home/y/yanliu/data/ruchi/51230610_000076_F$(printf "%04d" $((0 + run))).mid.gz synthetic_pulses_20230610_YL.root
done

# For physics triggers
#for run in {1..100}
#do
#  echo "Generating dump $((25 + run))"
#  python3 /sdf/home/y/yanliu/notebooks/synthetic/ruchi/Make_templates.py
#  pwd
#  ./AddSalt.exe /sdf/home/y/yanliu/data/raw/21210907_152363_F0010.mid.gz /sdf/home/y/yanliu/data/ruchi/51230610_000076_F$(printf "%04d" $((25 + run))).mid.gz synthetic_pulses_20230610_YL.root
#done
